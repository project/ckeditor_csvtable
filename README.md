# CK Editor CSV Table

This module provides a button for the CKEditor that allows the conversion of 
CSV data into a table within the WYSIWYG, as well as editing existing 
tables as CSV.

## REQUIREMENTS

* Drupal 8 or 9

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## CONFIGURATION

Drag the `Create Table from CSV` button onto the CKeditor within your text 
format and save.

<?php

namespace Drupal\Tests\ckeditor_csvtable\FunctionalJavascript;

use Drupal\editor\Entity\Editor;
use Drupal\filter\Entity\FilterFormat;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\ckeditor\Traits\CKEditorTestTrait;

/**
 * Tests the csvtable ckeditor plugin.
 *
 * @group ckeditor
 */
class CkeditorCsvTableTest extends WebDriverTestBase {

  use CKEditorTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'text',
    'ckeditor_csvtable',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FilterFormat::create([
      'format' => 'test_format',
      'name' => 'Test format',
      'filters' => [],
    ])->save();
    Editor::create([
      'editor' => 'ckeditor',
      'format' => 'test_format',
      'settings' => [
        'toolbar' => [
          'rows' => [
            [
              [
                'name' => 'Tables',
                'items' => [
                  'Table',
                  'csvtable',
                ],
              ],
            ],
          ],
        ],
      ],
    ])->save();

    $this->drupalCreateContentType(['type' => 'blog']);

    // Note that media_install() grants 'view media' to all users by default.
    $user = $this->drupalCreateUser([
      'use text format test_format',
      'create blog content',
    ]);

    $this->drupalLogin($user);
  }

  /**
   * Tests using `csvtable` button to add table to CKEditor from CSV.
   */
  public function testButton() {
    $this->drupalGet('/node/add/blog');
    $this->waitForEditor();

    // Press the `Create Table from CSV` button.
    $this->pressEditorButton('csvtable');
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Wait for the dialog to appear.
    $assert_session->waitForElementVisible('css', '.cke_dialog_body');

    $csv = <<<EOD
First,Last,Street,City,State,Zip
John,Doe,120 jefferson st.,Riverside, NJ, 08075
Jack,McGinnis,220 hobo Av.,Phila, PA,09119
"John ""Da Man""",Repici,120 Jefferson St.,Riverside, NJ,08075
Stephen,Tyler,"7452 Terrace ""At the Plaza"" road",SomeTown,SD, 91234
,Blankman,,SomeTown, SD, 00298
"Joan ""the bone"", Anne",Jet,"9th, at Terrace plc",Desert City,CO,00123
EOD;

    $page->fillField('CSV data', $csv);

    // Submit the dialog.
    $assert_session->elementExists('css', '.cke_dialog_footer')->pressButton('OK');

    // Since the buttons are in a different iframe, we need to switch
    // to the iframe that holds the CKEditor content.
    $this->assignNameToCkeditorIframe();
    $this->getSession()->switchToIFrame('ckeditor');

    // Assert the table is present in the CKEditor and has the expected
    // number of rows.
    $table = $assert_session->waitForElementVisible('css', '.cke_editable table');
    $this->assertCount(7, $table->findAll('css', 'tr'));

    // Test the "Edit Table as CSV" feature.
    $this->openContextMenu();
    $this->assignNameToCkeditorPanelIframe();
    $this->getSession()->switchToIFrame('panel');

    // Assert the "Edit Table as CSV" link is present, and then click it.
    $context_link = $this->assertSession()->elementExists('xpath', '//a[@aria-label="Edit Table as CSV"]');
    $context_link->click();

    $this->getSession()->switchToIFrame();
    $assert_session->waitForElementVisible('css', '.cke_dialog_body');

    // Assert table has been converted back to CSV for editing in the dialog.
    $csv_data_field = $assert_session->waitForField('CSV data');
    $value = $csv_data_field->getValue();
    $this->assertStringContainsString('First,Last,Street,City,State,Zip', $value);
    $value = str_replace('McGinnis', 'Hello World', $value);
    $csv_data_field->setValue($value);

    // Submit the dialog.
    $assert_session->elementExists('css', '.cke_dialog_footer')->pressButton('OK');

    $this->getSession()->switchToIFrame('ckeditor');

    // Assert the change to the CSV data is present in the newly generated
    // table.
    $table = $assert_session->waitForElementVisible('css', '.cke_editable table');
    $table_text = $table->getText();
    $this->assertStringContainsString('Hello World', $table_text);
  }

  /**
   * Opens the context menu for the currently selected element.
   *
   * @param string $instance_id
   *   The CKEditor instance ID.
   */
  protected function openContextMenu($instance_id = 'edit-body-0-value') {
    $this->getSession()->switchToIFrame();
    $script = <<<JS
      (function() {
        var editor = CKEDITOR.instances["$instance_id"];
        editor.contextMenu.open(editor.getSelection().$)
      }());
JS;
    $this->getSession()->executeScript($script);
  }

  /**
   * Assigns a name to the CKEditor context menu iframe.
   *
   * Note that this iframe doesn't appear until context menu appears.
   *
   * @see \Behat\Mink\Session::switchToIFrame()
   */
  protected function assignNameToCkeditorPanelIframe() {
    $javascript = <<<JS
(function(){
  document.getElementsByClassName('cke_panel_frame')[0].id = 'panel';
})()
JS;
    $this->getSession()->evaluateScript($javascript);
  }

}

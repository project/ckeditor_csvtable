<?php

namespace Drupal\ckeditor_csvtable\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "csvtable" plugin.
 *
 * @CKEditorPlugin(
 *   id = "csvtable",
 *   label = @Translation("Create table from CSV"),
 *   module = "csvtable"
 * )
 *
 * @codeCoverageIgnore
 */
class CsvTable extends CKEditorPluginBase implements CKEditorPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'csvtable' => [
        'label' => t('Import CSV as table'),
        'image' => drupal_get_path('module', 'ckeditor_csvtable') . '/js/plugins/csvtable/icons/csvtable.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_csvtable') . '/js/plugins/csvtable/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'papaparse/papaparse',
    ];
  }

}

/**
 * @file
 * csvtable plugin.
 */

(function ($, Drupal, CKEDITOR) {
  "use strict";

  CKEDITOR.plugins.add("csvtable", {
    requires: "dialog",
    icons: "csvtable",
    hidpi: true,
    init: function (editor) {
      if (editor.blockless) return;

      editor.ui.addButton("csvtable", {
        label: "Create Table from CSV",
        command: "csvtable",
        toolbar: "insert",
      });

      editor.addCommand("csvtable", new CKEDITOR.dialogCommand("csvtable"));

      // Our dialog definition.
      CKEDITOR.dialog.add("csvtable", function (editor) {
        return {
          title: "Create Table from CSV",
          minWidth: 600,
          minHeight: 200,

          // Dialog window content definition.
          contents: [
            {
              // Definition of the Basic Settings dialog tab.
              id: "info",
              label: "Basic Settings",

              // The tab content.
              elements: [
                {
                  type: "textarea",
                  id: "csvdata",
                  label: "CSV data",

                  // Validation checking whether the field is not empty.
                  validate: CKEDITOR.dialog.validate.notEmpty(
                    "CSV data field cannot be empty."
                  ),
                },
              ],
            }
          ],

          onShow: function () {
            var dialog = this;

            var table = editor.elementPath().contains("table", 1);

            // Contextually set dialog title text based on context.
            var currentTitle = table
              ? "Edit Table as CSV"
              : "Create Table from CSV";
            $(dialog.parts.title.$).text(currentTitle);

            // Update CSV data if in edit mode.
            if (table) {
              var rows = table.find("tr");
              var tableData = [];
              for (var i = 0; i < rows.count(); i++) {
                var row = [],
                  cols = rows.getItem(i).$.querySelectorAll("td, th");
                for (var j = 0; j < cols.length; j++) {
                  row.push(cols[j].innerText);
                }
                tableData.push(row);
              }
              var csvString = Papa.unparse(tableData);
              dialog.setValueOf("info", "csvdata", csvString);
            }
          },
          onOk: function () {
            var dialog = this;

            var path = editor.elementPath(),
              table = path.contains("table", 1);


            var csvData = dialog.getValueOf("info", "csvdata");

            if (table) {
              var tableElement = table;
            } else {
              var tableElement = editor.document.createElement("table");
            }

            tableElement.setHtml(convertcsvtableRows(csvData));
            
            if (!table) {
              // Insert as HTML so other plugins can react.
              editor.insertHtml(tableElement.getOuterHtml());
            }
          },
        };
      });

      function createDef(def) {
        return CKEDITOR.tools.extend(def || {}, {
          contextSensitive: 1,
          refresh: function (editor, path) {
            this.setState(
              path.contains("table", 1)
                ? CKEDITOR.TRISTATE_OFF
                : CKEDITOR.TRISTATE_DISABLED
            );
          },
        });
      }

      editor.addCommand(
        "editAsCsv",
        createDef({
          exec: function (editor) {
            var path = editor.elementPath(),
              table = path.contains("table", 1);

            if (!table) return;

            editor.execCommand("csvtable");
          },
        })
      );

      // If the "menu" plugin is loaded, register the menu items.
      if (editor.addMenuItems) {
        editor.addMenuItems({
          editAsCsv: {
            label: "Edit Table as CSV",
            command: "editAsCsv",
            group: "table",
            order: 5,
            icon: 'csvtable'
          },
        });
      }

      // If the "contextmenu" plugin is loaded, register the listeners.
      if (editor.contextMenu) {
        editor.contextMenu.addListener(function () {
          // menu item state is resolved on commands.
          return {
            editAsCsv: CKEDITOR.TRISTATE_OFF,
          };
        });
      }
    },
  });

  /**
   * Converts `csvdata` parameter from dialog into table rows.
   *
   * @param {string} data
   *   The CSV data to parse.
   * @returns {string}
   *   The contents of the table as HTML.
   */
  function convertcsvtableRows(data) {
    var parsed = Papa.parse(data);
    var lines = parsed.data,
      output = [],
      i;

    // Add header.
    output.push("<thead><tr><th>" + lines[0].join("</th><th>") + "</th></tr></thead><tbody>");

    // Add other rows.
    for (i = 1; i < lines.length; i++)
      output.push("<tr><td>" + lines[i].join("</td><td>") + "</td></tr>");

    output = output.join("") + "</tbody>";

    return output;
  }
})(jQuery, Drupal, CKEDITOR);
